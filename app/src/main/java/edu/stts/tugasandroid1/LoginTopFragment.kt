package edu.stts.tugasandroid1

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import kotlinx.android.synthetic.main.fragment_login_top.*

class LoginTopFragment : Fragment() {

    private lateinit var listener:LoginTopActionListener

    companion object {
        fun newInstance(listener: LoginTopActionListener): LoginTopFragment {
            val fragment = LoginTopFragment()
            fragment.listener = listener
            return fragment
        }
    }

    interface LoginTopActionListener {
        fun onUserReady(user: User)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        // Get the custom view for this fragment layout
        val view = inflater!!.inflate(R.layout.fragment_login_top,container,false)

        // Return the fragment view/layout
        return view
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        btn_login.setOnClickListener {
            val username = input_username.text.toString()
            val password = input_password.text.toString()

            sendToOtherFragment(username, password)
        }
    }

    private fun sendToOtherFragment(username: String, password: String) {
        val user = User(
                username,
                password
        )
        listener.onUserReady(user)
    }

}